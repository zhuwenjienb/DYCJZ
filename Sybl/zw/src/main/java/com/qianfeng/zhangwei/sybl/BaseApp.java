package com.qianfeng.zhangwei.sybl;

import android.app.Application;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-14 10:59
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 */
public class BaseApp extends Application {
    private static BaseApp app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
    }

    public static BaseApp getInstance() {
        return app;
    }
}

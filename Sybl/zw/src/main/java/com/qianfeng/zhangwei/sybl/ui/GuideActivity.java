package com.qianfeng.zhangwei.sybl.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.qianfeng.zhangwei.sybl.R;

/**
 * 功能介绍引导界面
 */
public class GuideActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
    }
}

package com.qianfeng.zhangwei.sybl.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-14 15:16
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 */
public class HttpUtils {

    public enum Method {
        GET, POST
    }

    public static String post(Method method, String url, Map<String, String> params) {
        return "";
    }

    private static HttpURLConnection getHttpURLConnection(Method method, String url, HashMap<String, String> params) {
        HttpURLConnection connection = null;
        try {
            URL mUrl = new URL(url);
            connection = (HttpURLConnection) mUrl.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            switch (method) {
                case POST:
                    break;
                case GET:
                    break;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return connection;
    }


}

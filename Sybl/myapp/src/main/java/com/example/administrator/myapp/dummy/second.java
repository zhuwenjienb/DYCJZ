package com.example.administrator.myapp.dummy;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.administrator.myapp.R;

public class second extends ActionBarActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }

    public static Boolean getText(Context context) {
        Boolean b = false;
        SharedPreferences nba = context.getSharedPreferences("NBA", MODE_PRIVATE);
        boolean kid = nba.getBoolean("kid", true);
        int version = nba.getInt("version", 0);
        if (kid && version != getVersion(context)) {
            SharedPreferences.Editor edit = nba.edit();
            edit.putBoolean("kid", false);
            edit.putInt("version", getVersion(context));
            edit.commit();
            b = true;
        }
        return b;
    }

    public static int getVersion(Context context) {
        int Version = 0;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            Version = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return Version;
    }


}

package com.example.administrator.myapp;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;


public class MainActivity extends ActionBarActivity {
    private ActionBar supportActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getText();
        // ActionBar supportActionBar = getSupportActionBar();
        View inflate = getLayoutInflater().inflate(R.layout.activity_main, null);
        supportActionBar.setDisplayShowHomeEnabled(true);
        supportActionBar.setDisplayShowCustomEnabled(true);
        supportActionBar.setCustomView(inflate, new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

    }

    public void getText() {
        supportActionBar = getSupportActionBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}

package com.example.administrator.myapp;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.example.administrator.myapp.dummy.second;

/**
 * Created by Administrator on 15-6-18.
 */
public class BaseApp extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_black);
        ImageView viewById = (ImageView) findViewById(R.id.black_imgae);
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation ta = new TranslateAnimation(60f, 100f, 60f, 100f);
        ta.setDuration(1500);
        ta.setFillAfter(true);
        animationSet.addAnimation(ta);
        viewById.startAnimation(animationSet);
        initView();
    }

    private void initView() {
           if( second.getText(getApplicationContext())){
               new Intent(getApplication(),MainActivity.class);
           }
    }

}

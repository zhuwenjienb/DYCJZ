package com.example.administrator.viewpager;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {
    private ViewPager VP;
    private List<ImageView> list;
    private MyAdapter MA;
    private Button VB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        VP = (ViewPager) findViewById(R.id.MyViewPager);
        VB = (Button) findViewById(R.id.item_button);
        VB.setVisibility(View.GONE);
        initView();
        MA = new MyAdapter();
        VP.setAdapter(MA);
        VP.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                VB.setVisibility(View.GONE);
                if (position == 2) {
                    VB.setVisibility(View.VISIBLE);
//                    VB.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Toast.makeText(getApplication(), "终于做好了", Toast.LENGTH_LONG).show();
//                        }
//                    });
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initView() {
        list = new ArrayList<ImageView>();
        for (int i = 1; i <= 3; i++) {
            ImageView image = (ImageView) new ImageView(this);

            // image.setImageResource(getResources().getIdentifier("bg_guide_0" + i, "mipmap", getPackageName()));
            if (i == 1) {

                image.setImageResource(getResources().getIdentifier("bg_guide_0" + i, "mipmap", getPackageName()));
            } else if (i == 2) {
                image.setImageResource(getResources().getIdentifier("bg_guide_0" + i, "mipmap", getPackageName()));
            } else if (i == 3) {

                image.setImageResource(getResources().getIdentifier("bg_guide_0" + i, "mipmap", getPackageName()));


            }
            list.add(image);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class MyAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(list.get(position));
            return list.get(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(list.get(position));
        }
    }
}

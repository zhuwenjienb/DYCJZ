package com.example.administrator.sybl.bean;

/**
 * Created by Administrator on 15-6-16.
 */
public class Head_image_activity {

    /**
     * bimg : http://i3.72g.com/upload/201504/201504131746491911.png
     * bname : 活动头图
     * id : 1
     * px : 1
     * target : 3
     * target_id : 1
     */
    private String bimg;
    private String bname;
    private String id;
    private String px;
    private String target;
    private String target_id;

    public void setBimg(String bimg) {
        this.bimg = bimg;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPx(String px) {
        this.px = px;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public void setTarget_id(String target_id) {
        this.target_id = target_id;
    }

    public String getBimg() {
        return bimg;
    }

    public String getBname() {
        return bname;
    }

    public String getId() {
        return id;
    }

    public String getPx() {
        return px;
    }

    public String getTarget() {
        return target;
    }

    public String getTarget_id() {
        return target_id;
    }
}

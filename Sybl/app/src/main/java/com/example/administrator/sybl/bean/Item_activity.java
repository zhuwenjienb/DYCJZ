package com.example.administrator.sybl.bean;

/**
 * Created by Administrator on 15-6-16.
 */
public class Item_activity {

    /**
     * id : 11
     * status : 进行中
     * aetime : 2015-06-16 23:00
     * hotpic : http://i3.72g.com/upload/201506/201506110946195656.jpg
     * shortname : 《穿越吧！主公》公测活动
     * astime : 2015-06-11 09:00
     * aname : 中国版超级英雄手游《穿越吧！主公》 今日公测千万豪礼洒不停
     * total_join_user : 13
     */
    private String id;
    private String status;
    private String aetime;
    private String hotpic;
    private String shortname;
    private String astime;
    private String aname;
    private int total_join_user;

    public void setId(String id) {
        this.id = id;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setAetime(String aetime) {
        this.aetime = aetime;
    }

    public void setHotpic(String hotpic) {
        this.hotpic = hotpic;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public void setAstime(String astime) {
        this.astime = astime;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    public void setTotal_join_user(int total_join_user) {
        this.total_join_user = total_join_user;
    }

    public String getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public String getAetime() {
        return aetime;
    }

    public String getHotpic() {
        return hotpic;
    }

    public String getShortname() {
        return shortname;
    }

    public String getAstime() {
        return astime;
    }

    public String getAname() {
        return aname;
    }

    public int getTotal_join_user() {
        return total_join_user;
    }
}

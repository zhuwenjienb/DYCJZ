package com.example.administrator.sybl.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.administrator.sybl.R;
import com.example.administrator.sybl.adapter.Gift_adapter;
import com.example.administrator.sybl.adapter.Jf_adapter;
import com.example.administrator.sybl.bean.Gift;
import com.example.administrator.sybl.bean.Integration_activity;
import com.example.administrator.sybl.utils.HttpUtils;
import com.example.administrator.sybl.utils.UrlConstants;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 15-6-15.
 */
public class Jf_item1_fragment extends Fragment implements AdapterView.OnItemClickListener, PullToRefreshBase.OnRefreshListener2<ListView> {
    private Map<String, String> map;
    private int page = 1;
    private int gifttype = 1;
    private List<Integration_activity> list = new ArrayList<>();
    private PullToRefreshListView ptrListView;
    private Jf_adapter gift_adapter;
    private View view;
    private ListView listView;
    private List<Integration_activity> gifts;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.jf_fulllistview, null);
        // View inflate = inflater.inflate(R.layout.activity_gift_item1, null);
        map = new HashMap<>();

        map.put("type", "1");
        map.put("page", "" + page);

        ptrListView = (PullToRefreshListView) view.findViewById(R.id.fragment_tuan_ptrListview);
        ptrListView.setMode(PullToRefreshBase.Mode.BOTH);
        ptrListView.setOnRefreshListener(this);

        /**
         * ��ȡϵͳ�Դ�ListView
         */
        listView = ptrListView.getRefreshableView();
        //  listView.addHeaderView(headVRelativeLayoutiew);

        gift_adapter = new Jf_adapter(list, getActivity());
        ptrListView.setAdapter(gift_adapter);
        //�����Զ�ˢ�� ��Ҫע�͵� pulltorefreshListView Դ��71-75�д���
        ptrListView.setRefreshing();
        return view;
    }

    private void loadData() {
        new MyTask().execute(UrlConstants.URL_JF);
    }


    @Override
    public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
        page = 1;
        loadData();
    }

    @Override
    public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
        page++;
        loadData();

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    public class MyTask extends AsyncTask<String, Void, List<Integration_activity>> {

        @Override
        protected List<Integration_activity> doInBackground(String... params) {
            try {
                String post = HttpUtils.post(params[0], map);

                JSONObject jsonObject = new JSONObject(post);
                JSONArray info = jsonObject.optJSONArray("info");
                Log.i("info", "======3=====" + info.length());
                List<Integration_activity> gf = new ArrayList<>();
                for (int i = 0; i < info.length(); i++) {
                    Integration_activity gift = new Integration_activity();
                    JSONObject jsonObject1 = info.optJSONObject(i);
                    //物品名称
                    gift.setName(jsonObject1.optString("name"));
                    //剩余QB
                    gift.setConsume(jsonObject1.optString("consume"));
                    //图片
                    gift.setIcon(jsonObject1.optString("icon"));
                    //剩余数量
                    gift.setRemain(jsonObject1.optString("remain"));
                    gf.add(gift);

                }
                return gf;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Integration_activity> strings) {

            super.onPostExecute(strings);
            if (page == 1 && !list.isEmpty()) {
                list.clear();
            }
            Log.i("info", "======2222=========" + strings);
            list.addAll(strings);
            // Log.i("info", "===============" + list);
            gift_adapter.notifyDataSetChanged();
            ptrListView.onRefreshComplete();
        }
    }

}
package com.example.administrator.sybl.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-15 11:10
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 */
public class AppUtils {


    public static boolean isFirst(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("isfirst", Context.MODE_PRIVATE);
        int version = sharedPreferences.getInt("version", 0);
        boolean isFirst = sharedPreferences.getBoolean("first", true);
        if (version != getVersion(context) || isFirst) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("first", false);
            editor.putInt("version", getVersion(context));
            editor.commit();
        }
        return isFirst;
    }

    public static int getVersion(Context context) {
        int version = 0;

        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return version;
    }


}
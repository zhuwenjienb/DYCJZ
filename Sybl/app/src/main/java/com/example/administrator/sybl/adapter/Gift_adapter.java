package com.example.administrator.sybl.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.sybl.R;
import com.example.administrator.sybl.bean.Gift;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.List;

/**
 * Created by Administrator on 15-6-15.
 */
public class Gift_adapter extends BaseAdapter {
    private List<Gift> list;
    private Context context;
    private LayoutInflater inflater;
    private DisplayImageOptions options;

    public Gift_adapter(List<Gift> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
        DisplayImageOptions.Builder builder = new DisplayImageOptions.Builder();
        builder.showImageOnLoading(R.mipmap.ic_launcher);
        builder.showImageOnFail(R.mipmap.ic_launcher);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.indicator_arrow) // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.drawable.indicator_arrow) // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.drawable.indicator_arrow) // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true) // 设置下载的图片是否缓存在内存中
                .cacheOnDisk(true) // 设置下载的图片是否缓存在SD卡中
                .displayer(new RoundedBitmapDisplayer(30)) // 设置成圆角图片
                .build(); // 构建完成
//        options = new DisplayImageOptions.Builder()
//                .showImageOnLoading(R.drawable.indicator_arrow) //设置图片在下载期间显示的图片
//                .showImageForEmptyUri(R.drawable.indicator_arrow)//设置图片Uri为空或是错误的时候显示的图片
//                .showImageOnFail(R.drawable.indicator_arrow)  //设置图片加载/解码过程中错误时候显示的图片
//                .cacheInMemory(true)//设置下载的图片是否缓存在内存中
//                .cacheOnDisc(true)//设置下载的图片是否缓存在SD卡中
//                .considerExifParams(true)  //是否考虑JPEG图像EXIF参数（旋转，翻转）
//                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)//设置图片以如何的编码方式显示
//                .bitmapConfig(Bitmap.Config.RGB_565)//设置图片的解码类型//
////.delayBeforeLoading(int delayInMillis)//int delayInMillis为你设置的下载前的延迟时间
////设置图片加入缓存前，对bitmap进行设置
////.preProcessor(BitmapProcessor preProcessor)
//                .resetViewBeforeLoading(true)//设置图片在下载前是否重置，复位
//                .displayer(new RoundedBitmapDisplayer(20))//是否设置为圆角，弧度为多少
//                .displayer(new FadeInBitmapDisplayer(100))//是否图片加载好后渐入的动画时间
//                .build();//构建完成

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.gift_fragment, null);
            vh = new ViewHolder();
            vh.gift_image = (ImageView) convertView.findViewById(R.id.gift_image);
            vh.gift_title = (TextView) convertView.findViewById(R.id.gift_title);
            vh.gift_context_text = (TextView) convertView.findViewById(R.id.gift_context_text);
            vh.gift_end_text = (TextView) convertView.findViewById(R.id.gift_end_text);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        Gift gift = list.get(position);
        vh.gift_title.setText(gift.getName());
        vh.gift_context_text.setText("剩下�:" + gift.getRemain());
        vh.gift_end_text.setText(gift.getContent());
        ImageLoader.getInstance().displayImage(list.get(position).getIcon(), vh.gift_image, options);
        return convertView;
    }

    public class ViewHolder {
        public ImageView gift_image;
        public TextView gift_title, gift_context_text, gift_end_text;
    }
}

package com.example.administrator.sybl.utils;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Administrator on 15-6-15.
 */
public class UrlConstants {
    public static final String URL_GIFT = "http://zhushou.72g.com/app/gift/gift_list";
    public static final String URL_ACTIVITY = "http://zhushou.72g.com/app/activity/activity_list";
    public static final String URL_MONEY = "http://zhushou.72g.com/app/game/game_list";
    public static final String URL_HEAD_IMAGE = "http://zhushou.72g.com/app/common/banner_info";
    public static final String URL_JF = "http://zhushou.72g.com/app/prize/prize_list";
    public static final String URL_XQ = "http://zhushou.72g.com/app/gift/gift_info";
    public static final String URL_YX = "http://zhushou.72g.com/app/game/game_info/";
}

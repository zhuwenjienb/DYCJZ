package com.example.administrator.sybl.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.administrator.sybl.R;
import com.example.administrator.sybl.bean.XQ_gift;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.List;

/**
 * Created by Administrator on 15-6-18.
 */
public class XQAdapter extends BaseAdapter {
    public List<XQ_gift> list;
    public Context context;
    public LayoutInflater inflater;
    private DisplayImageOptions options;

    public XQAdapter(Context context, List<XQ_gift> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.indicator_arrow) // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.drawable.indicator_arrow) // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.drawable.indicator_arrow) // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true) // 设置下载的图片是否缓存在内存中
                .cacheOnDisk(true) // 设置下载的图片是否缓存在SD卡中
                .displayer(new RoundedBitmapDisplayer(30)) // 设置成圆角图片
                .build(); // 构建完成
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHodler VH = null;
        if (view == null) {
            view = inflater.inflate(R.layout.xq_activity, null);
            VH = new ViewHodler();
            VH.xq_image = (ImageView) view.findViewById(R.id.xq_image);
            VH.image_below_text = (TextView) view.findViewById(R.id.image_below_text);
            VH.image_right_text = (TextView) view.findViewById(R.id.image_right_text);
            VH.progress_right_text_below = (TextView) view.findViewById(R.id.progress_right_text_below);
            VH.end_right_text_below = (TextView) view.findViewById(R.id.end_right_text_below);
            VH.time_right_text_below = (TextView) view.findViewById(R.id.time_right_text_below);
            VH.time_end_text_below = (TextView) view.findViewById(R.id.time_end_text_below);
            VH.context_text_below = (TextView) view.findViewById(R.id.context_text_below);
            VH.end_below = (TextView) view.findViewById(R.id.end_below);
            VH.progress = (ProgressBar) view.findViewById(R.id.progress);
            view.setTag(VH);
        } else {
            VH = (ViewHodler) view.getTag();
        }
        XQ_gift xq_gift = list.get(i);
        VH.image_below_text.setText(xq_gift.getConsume() + "Q币");
        VH.image_right_text.setText(xq_gift.getName());
        Integer integer = Integer.valueOf(xq_gift.getRemain());
        String total = xq_gift.getTotal();
        VH.progress.setProgress(integer);
        VH.progress.setMax(Integer.valueOf(total));
        VH.progress_right_text_below.setText(xq_gift.getRemain());
        VH.end_right_text_below.setText(total);
        VH.time_right_text_below.setText(xq_gift.getStime());
        VH.time_end_text_below.setText(xq_gift.getEtime());
        VH.context_text_below.setText(xq_gift.getContent());
        VH.end_below.setText(xq_gift.getHowget());
        ImageLoader.getInstance().displayImage(xq_gift.getIcon(), VH.xq_image, options);
        return view;
    }

    public class ViewHodler {


        private TextView image_below_text, image_right_text, progress_right_text_below;
        private TextView end_right_text_below, time_right_text_below, time_end_text_below, context_text_below, end_below;
        private ImageView xq_image;
        private ProgressBar progress;
    }
}

package com.example.administrator.sybl.bean;

/**
 * Created by Administrator on 15-6-16.
 */
public class Money {

    /**
     * id : 46
     * icon : http://i3.72g.com/upload/201506/201506111628537395.jpg
     * ios_dl : 910395494
     * dl_back_jifen : 0.4
     * count_dl : 7547
     * name : 少年三国志
     * android_dl : http://down.72g.com/new/snsgz.apk
     * score : 6.7
     * size : 89.74MB
     */
    private String id;
    private String icon;
    private String ios_dl;
    private String dl_back_jifen;
    private String count_dl;
    private String name;
    private String android_dl;
    private String score;
    private String size;

    public void setId(String id) {
        this.id = id;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setIos_dl(String ios_dl) {
        this.ios_dl = ios_dl;
    }

    public void setDl_back_jifen(String dl_back_jifen) {
        this.dl_back_jifen = dl_back_jifen;
    }

    public void setCount_dl(String count_dl) {
        this.count_dl = count_dl;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAndroid_dl(String android_dl) {
        this.android_dl = android_dl;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getId() {
        return id;
    }

    public String getIcon() {
        return icon;
    }

    public String getIos_dl() {
        return ios_dl;
    }

    public String getDl_back_jifen() {
        return dl_back_jifen;
    }

    public String getCount_dl() {
        return count_dl;
    }

    public String getName() {
        return name;
    }

    public String getAndroid_dl() {
        return android_dl;
    }

    public String getScore() {
        return score;
    }

    public String getSize() {
        return size;
    }
}

package com.example.administrator.sybl.fragment;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.administrator.sybl.R;
import com.example.administrator.sybl.adapter.Gift_adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 15-6-15.
 */
public class Mygift_fragment extends Fragment implements ViewPager.OnPageChangeListener {

    private ListView listView;
    private View view;
    private View view2;
    private RadioGroup rg;
    private Gift_adapter gift_fragment;

    private int page = 1;
    private Map<String, String> map;
    private int gifttype = 1;
    private ViewPager vp;
    private FragmentTransaction fragmentTransaction;
    private List<Fragment> list_fragment;
    private MyVPAdapter ViewPage_adapter;
    private Jf_item1_fragment gift_item1_fragment = new Jf_item1_fragment();
    private Jf_item2_fragment gift_item2_fragment = new Jf_item2_fragment();


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //view = inflater.inflate(R.layout.activity_fulllistview, null);
        view2 = inflater.inflate(R.layout.activity_myitem, null);
        list_fragment = new ArrayList<>();
        list_fragment.add(gift_item1_fragment);
        list_fragment.add(gift_item2_fragment);
        vp = (ViewPager) view2.findViewById(R.id.jf_guide_viewpager);
        ViewPage_adapter = new MyVPAdapter(getFragmentManager());
        vp.setAdapter(ViewPage_adapter);
        vp.setOnPageChangeListener(this);
        rg = (RadioGroup) view2.findViewById(R.id.jf_radioGroup);
        vp.setCurrentItem(0);
//        ActionBar actionBar = getActivity().getActionBar();
//        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
//        actionBar.addTab(actionBar.newTab().setTag("礼包大放送"));
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.jf_radioGroup_radio1:
                        gifttype = 1;
                        RadioButton rb3 = (RadioButton) view2.findViewById(R.id.jf_radioGroup_radio1);
                        rb3.setTextColor(Color.WHITE);
                        RadioButton rb4 = (RadioButton) view2.findViewById(R.id.jf_radioGroup_radio2);
                        rb4.setTextColor(Color.BLUE);
                        vp.setCurrentItem(0);
                        break;
                    case R.id.jf_radioGroup_radio2:
                        gifttype = 2;

                        RadioButton rb1 = (RadioButton) view2.findViewById(R.id.jf_radioGroup_radio1);
                        rb1.setTextColor(Color.BLUE);
                        RadioButton rb2 = (RadioButton) view2.findViewById(R.id.jf_radioGroup_radio2);
                        rb2.setTextColor(Color.WHITE);
                        vp.setCurrentItem(1);
                        break;

                }
            }
        });
        return view2;
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        RadioButton RB = (RadioButton) rg.getChildAt(position);
        RB.setChecked(true);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    public class MyVPAdapter extends FragmentPagerAdapter {


        public MyVPAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return list_fragment.get(position);
        }

        @Override
        public int getCount() {
            return list_fragment.size();
        }
    }
}

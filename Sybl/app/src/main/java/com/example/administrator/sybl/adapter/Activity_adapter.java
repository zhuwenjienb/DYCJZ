package com.example.administrator.sybl.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.sybl.R;
import com.example.administrator.sybl.bean.Gift;
import com.example.administrator.sybl.bean.Item_activity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.List;

/**
 * Created by Administrator on 15-6-16.
 */
public class Activity_adapter extends BaseAdapter {
    public List<Item_activity> list;
    private Context context;
    private LayoutInflater inflater;
    private DisplayImageOptions options;

    public Activity_adapter(Context context, List<Item_activity> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
        DisplayImageOptions.Builder builder = new DisplayImageOptions.Builder();
        builder.showImageOnLoading(R.mipmap.ic_launcher);
        builder.showImageOnFail(R.mipmap.ic_launcher);
        // options = builder.build();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.indicator_arrow) // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.drawable.indicator_arrow) // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.drawable.indicator_arrow) // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true) // 设置下载的图片是否缓存在内存中
                .cacheOnDisk(true) // 设置下载的图片是否缓存在SD卡中
                .displayer(new RoundedBitmapDisplayer(30)) // 设置成圆角图片
                .build(); // 构建完成

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_activity, null);
            vh = new ViewHolder();
            vh.gift_image = (ImageView) convertView.findViewById(R.id.item_activity_image);
            vh.gift_title = (TextView) convertView.findViewById(R.id.item_activity_title);
            vh.gift_context_text = (TextView) convertView.findViewById(R.id.item_activity_context_text);
            vh.gift_end_text = (TextView) convertView.findViewById(R.id.item_activity_end_text);
            vh.item_activity_end_left_text = (TextView) convertView.findViewById(R.id.item_activity_end_left_text);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        Item_activity gift = list.get(position);
        vh.gift_title.setText(gift.getShortname());
        vh.gift_context_text.setText(gift.getAname());
        vh.gift_end_text.setText("" + gift.getTotal_join_user() + "人参加");
        vh.item_activity_end_left_text.setText(gift.getStatus());
        ImageLoader.getInstance().displayImage(list.get(position).getHotpic(), vh.gift_image, options);
        return convertView;
    }

    public class ViewHolder {
        public ImageView gift_image;
        public TextView gift_title, gift_context_text, gift_end_text, item_activity_end_left_text;
    }
}

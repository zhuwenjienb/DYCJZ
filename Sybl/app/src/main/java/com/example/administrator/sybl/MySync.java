package com.example.administrator.sybl;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;

import com.alibaba.fastjson.JSON;
import com.example.administrator.sybl.adapter.XQAdapter;
import com.example.administrator.sybl.bean.XQ_gift;
import com.example.administrator.sybl.utils.HttpUtils;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 15-6-18.
 */
public class MySync extends AsyncTask<String, Void, List<XQ_gift>> {
    private Map<String, String> map;
    private VHolder VH;
    private XQAdapter xqAdapter;
    private ListView webView;
    private List<XQ_gift> list;
    private ImageView image;

    public MySync(Map<String, String> map, VHolder VH) {
        this.map = map;
        this.VH = VH;
    }

    @Override
    protected List<XQ_gift> doInBackground(String... strings) {

        String post = null;
        try {
            post = HttpUtils.post(strings[0], map);
            Log.i("朱文杰", "==========朱文杰1234============" + post);
            JSONObject jsonObject = new JSONObject(post);
            JSONObject info = jsonObject.optJSONObject("info");
            list = new ArrayList<>();
            XQ_gift xq = new XQ_gift();
            String name = info.optString("name");
            xq.setName(name);

            xq.setIcon(info.optString("icon"));
            xq.setTotal(info.getString("total"));
            xq.setRemain(info.getString("remain"));
            xq.setStime(info.getString("stime"));
            xq.setEtime(info.getString("etime"));
            xq.setConsume(info.getString("consume"));
            xq.setContent(info.getString("content"));
            xq.setHowget(info.getString("howget"));
            list.add(xq);
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }

        //List<XQ_gift> xq_gifts = JSON.parseArray(post, XQ_gift.class);

        return null;

    }


    @Override
    protected void onPostExecute(List<XQ_gift> xq_gifts) {
        super.onPostExecute(xq_gifts);
        VH.send(xq_gifts);
    }

    public interface VHolder {

        public Void send(List<XQ_gift> xq_gifts);
    }
}

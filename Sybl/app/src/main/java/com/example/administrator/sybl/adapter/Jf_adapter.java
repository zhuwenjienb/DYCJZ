package com.example.administrator.sybl.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.sybl.R;
import com.example.administrator.sybl.bean.Gift;
import com.example.administrator.sybl.bean.Integration_activity;
import com.example.administrator.sybl.bean.Item_activity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.List;

/**
 * Created by Administrator on 15-6-15.
 */
public class Jf_adapter extends BaseAdapter {
    private List<Integration_activity> list;
    private Context context;
    private LayoutInflater inflater;
    private DisplayImageOptions options;

    public Jf_adapter(List<Integration_activity> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
        DisplayImageOptions.Builder builder = new DisplayImageOptions.Builder();
        builder.showImageOnLoading(R.mipmap.ic_launcher);
        builder.showImageOnFail(R.mipmap.ic_launcher);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.indicator_arrow) // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.drawable.indicator_arrow) // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.drawable.indicator_arrow) // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true) // 设置下载的图片是否缓存在内存中
                .cacheOnDisk(true) // 设置下载的图片是否缓存在SD卡中
                .displayer(new RoundedBitmapDisplayer(30)) // 设置成圆角图片
                .build(); // 构建完成

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.activity_jf, null);
            vh = new ViewHolder();


            //  jf_imgae  jf_title   jf_title  jf_num  jf_button

            vh.gift_image = (ImageView) convertView.findViewById(R.id.jf_imgae);
            vh.gift_title = (TextView) convertView.findViewById(R.id.jf_title);
            vh.gift_context_text = (TextView) convertView.findViewById(R.id.jf_num);
            vh.btn = (Button) convertView.findViewById(R.id.jf_button);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        Integration_activity gift = list.get(position);
        vh.gift_title.setText(gift.getName());
        vh.gift_context_text.setText(gift.getRemain());
        String consume = gift.getConsume();
        if (consume.equals("30")) {
            vh.btn.setText(gift.getConsume() + "Q币");
            vh.btn.setEnabled(false);
        } else {
            vh.btn.setText(gift.getConsume() + "Q币");
        }

        ImageLoader.getInstance().displayImage(list.get(position).getIcon(), vh.gift_image, options);
        return convertView;
    }

    public class ViewHolder {
        public ImageView gift_image;
        public Button btn;
        public TextView gift_title, gift_context_text, gift_end_text;
    }
}

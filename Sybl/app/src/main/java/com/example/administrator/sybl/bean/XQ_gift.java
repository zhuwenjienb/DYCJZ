package com.example.administrator.sybl.bean;

/**
 * Created by Administrator on 15-6-18.
 */
public class XQ_gift {

    /**
     * total : 100
     * icon : http://i3.72g.com/upload/201506/201506161121469616.jpg
     * platform : 3
     * ios_dl : 895691221
     * game_type : 角色扮演
     * stime : 2015-06-16
     * android_dl : http://down.72g.com/new/tjxmcps.apk
     * size : 167M
     * consume : 0.9
     * id : 3625
     * content : 50钻石
     黑市交易令*3
     10万金币*1
     精炼石*5
     龙卷符文*1
     * gamename : 太极熊猫
     * remain : 75
     * etime : 2015-07-16
     * howget : 进入游戏，屏幕右侧：系统--礼包兑换--输入兑换码（可复制）--领取即可，领取成功即可在邮件内领取相应附件，领取失败会有对应提示。
     （已领取礼包可在个人信息-“我的礼包”中查看，领取到的礼包请尽快使用避免其过久未使用进入淘号。）
     * game_id : 4
     * name : 《太极熊猫》新版本公会争霸礼包
     */
    private String total;
    private String icon;
    private String platform;
    private String ios_dl;
    private String game_type;
    private String stime;
    private String android_dl;
    private String size;
    private String consume;
    private String id;
    private String content;
    private String gamename;
    private String remain;
    private String etime;
    private String howget;
    private String game_id;
    private String name;

    public void setTotal(String total) {
        this.total = total;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public void setIos_dl(String ios_dl) {
        this.ios_dl = ios_dl;
    }

    public void setGame_type(String game_type) {
        this.game_type = game_type;
    }

    public void setStime(String stime) {
        this.stime = stime;
    }

    public void setAndroid_dl(String android_dl) {
        this.android_dl = android_dl;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setConsume(String consume) {
        this.consume = consume;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setGamename(String gamename) {
        this.gamename = gamename;
    }

    public void setRemain(String remain) {
        this.remain = remain;
    }

    public void setEtime(String etime) {
        this.etime = etime;
    }

    public void setHowget(String howget) {
        this.howget = howget;
    }

    public void setGame_id(String game_id) {
        this.game_id = game_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotal() {
        return total;
    }

    public String getIcon() {
        return icon;
    }

    public String getPlatform() {
        return platform;
    }

    public String getIos_dl() {
        return ios_dl;
    }

    public String getGame_type() {
        return game_type;
    }

    public String getStime() {
        return stime;
    }

    public String getAndroid_dl() {
        return android_dl;
    }

    public String getSize() {
        return size;
    }

    public String getConsume() {
        return consume;
    }

    public String getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public String getGamename() {
        return gamename;
    }

    public String getRemain() {
        return remain;
    }

    public String getEtime() {
        return etime;
    }

    public String getHowget() {
        return howget;
    }

    public String getGame_id() {
        return game_id;
    }

    public String getName() {
        return name;
    }
}

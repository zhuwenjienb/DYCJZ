package com.example.administrator.sybl.bean;

/**
 * Created by Administrator on 15-6-17.
 */
public class Integration_activity {

    /**
     * id : 14
     * icon : http://i3.72g.com/upload/201504/201504131803099807.jpg
     * remain : 5
     * name : iPhone 6 Plus 16GB
     * prize_type : 2
     * consume : 6088
     */
    private String id;
    private String icon;
    private String remain;
    private String name;
    private String prize_type;
    private String consume;

    public void setId(String id) {
        this.id = id;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setRemain(String remain) {
        this.remain = remain;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrize_type(String prize_type) {
        this.prize_type = prize_type;
    }

    public void setConsume(String consume) {
        this.consume = consume;
    }

    public String getId() {
        return id;
    }

    public String getIcon() {
        return icon;
    }

    public String getRemain() {
        return remain;
    }

    public String getName() {
        return name;
    }

    public String getPrize_type() {
        return prize_type;
    }

    public String getConsume() {
        return consume;
    }
}

package com.example.administrator.sybl.fragment;

import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.administrator.sybl.R;
import com.example.administrator.sybl.adapter.Activity_adapter;
import com.example.administrator.sybl.adapter.Main_adapter;
import com.example.administrator.sybl.bean.Gift;
import com.example.administrator.sybl.bean.Head_image_activity;
import com.example.administrator.sybl.bean.Item_activity;
import com.example.administrator.sybl.utils.HttpUtils;
import com.example.administrator.sybl.utils.UrlConstants;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.InterfaceAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 15-6-15.
 */
public class Activity_fragment extends Fragment implements PullToRefreshBase.OnRefreshListener2<ListView> {
    private Map<String, String> map;
    private Map<String, String> map2;
    private int page = 1;
    private int gifttype = 1;
    private List<Item_activity> list = new ArrayList<>();
    private PullToRefreshListView ptrListView;
    private Activity_adapter activity_adapter;
    private View view;
    private ListView listView;
    private List<Gift> gifts;
    private List<ImageView> list_image = new ArrayList<ImageView>();
    private ViewPager vp;
    private Main_adapter main_adapter;
    private List<ImageView> lb_image = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.item_activity_fulllistview, null);
        // View inflate = inflater.inflate(R.layout.activity_gift_item1, null);
        map = new HashMap<>();
        map.put("uid", "");
        map.put("token", "78fc45f6f%2C7%2C9");
        map.put("platform", "2");
        map.put("gifttype", "1");
        map.put("page", "" + page);
        map2 = new HashMap<>();
        map2.put("platform", "2");
        map2.put("pos", "1");
        map2.put("compare", "65dcae2018c98b3a96f8263d05e3538c");


        ptrListView = (PullToRefreshListView) view.findViewById(R.id.fragment_tuan_ptrListview);
        ptrListView.setMode(PullToRefreshBase.Mode.BOTH);
        ptrListView.setOnRefreshListener(this);

        /**
         * 获取系统自带ListView
         */

        new MySync(inflater).execute(UrlConstants.URL_HEAD_IMAGE);

//        listView = ptrListView.getRefreshableView();
//
//        listView.addHeaderView(initView(inflater));
        activity_adapter = new Activity_adapter(getActivity(), list);
        ptrListView.setAdapter(activity_adapter);
        //设置自动刷新 需要注释掉 pulltorefreshListView 源码71-75行代码
        ptrListView.setRefreshing();
        return view;
    }


//    private View initView(LayoutInflater inflater) {
//        View vf = inflater.inflate(R.layout.head_activity, null);
//        vp = (ViewPager) vf.findViewById(R.id.activity_guide_viewpager);
//        main_adapter = new Main_adapter(list_image);
//        vp.setAdapter(main_adapter);
//
//        return vf;
//    }

    private void loadData() {
        new MyTask().execute(UrlConstants.URL_ACTIVITY);
    }


    @Override
    public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
        page = 1;
        loadData();
    }

    @Override
    public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
        page++;
        loadData();

    }

    public class MyTask extends AsyncTask<String, Void, List<Item_activity>> {

        @Override
        protected List<Item_activity> doInBackground(String... params) {
            try {
                String post = HttpUtils.post(params[0], map);

                JSONObject jsonObject = new JSONObject(post);
                JSONArray info = jsonObject.optJSONArray("info");
                //  Log.i("info", "======3=====" + info.length());
                List<Item_activity> gf = new ArrayList<>();
                for (int i = 0; i < info.length(); i++) {
                    Item_activity gift = new Item_activity();
                    JSONObject jsonObject1 = info.optJSONObject(i);
                    gift.setShortname(jsonObject1.optString("shortname"));
                    gift.setAname(jsonObject1.optString("aname"));
                    gift.setHotpic(jsonObject1.optString("hotpic"));
                    gift.setTotal_join_user(jsonObject1.optInt("total_join_user"));
                    gift.setStatus(jsonObject1.optString("status"));
                    gf.add(gift);

                }
                return gf;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Item_activity> strings) {

            super.onPostExecute(strings);
            if (page == 1 && !list.isEmpty()) {
                list.clear();
            }
            //Log.i("info", "======2222=========" + strings);
            list.addAll(strings);
            // Log.i("info", "===============" + list);
            activity_adapter.notifyDataSetChanged();
            ptrListView.onRefreshComplete();
        }
    }


    public class MySync extends AsyncTask<String, Void, List<ImageView>> {
        private LayoutInflater inflater;

        //
        public MySync(LayoutInflater inflater) {
            this.inflater = inflater;
        }

        @Override
        protected List<ImageView> doInBackground(String... params) {
            try {
                String post = HttpUtils.post(params[0], map2);

                JSONObject jsonObject = new JSONObject(post);
                JSONArray info = jsonObject.optJSONArray("info");


                for (int i = 0; i < info.length(); i++) {
                    JSONObject jsonObject1 = info.optJSONObject(i);
                    String bimg = jsonObject1.optString("bimg");

                    ImageView imageView = new ImageView(getActivity());
                    ViewGroup.LayoutParams VL = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    imageView.setLayoutParams(VL);
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    ImageLoader.getInstance().displayImage(bimg, imageView);
//                    byte[] aByte = HttpUtils.getByte(bimg);
//                    imageView.setImageBitmap(BitmapFactory.decodeByteArray(aByte, 0, aByte.length));
                    list_image.add(imageView);

                    Log.i("bimg", "==========朱文杰==============" + bimg);
                }


                // Log.i("info", "==========朱文杰==============" + list_image.size());
                return list_image;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<ImageView> imageViews) {
            super.onPostExecute(imageViews);
            //  onCallback.OnCallback(imageViews);

            // View vf = inflater.inflate(R.layout.head_activity, null);x
            View vf = inflater.inflate(R.layout.head_activity, null);
            vp = (ViewPager) vf.findViewById(R.id.activity_guide_viewpager);
            main_adapter = new Main_adapter(imageViews);
            vp.setAdapter(main_adapter);
            listView = ptrListView.getRefreshableView();
            Log.i("info", "==========朱文杰==============" + imageViews.size());
            listView.addHeaderView(vf);
            ptrListView.onRefreshComplete();
        }


    }


}



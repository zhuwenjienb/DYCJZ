package com.example.administrator.sybl.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.example.administrator.sybl.R;
import com.example.administrator.sybl.adapter.GuidePageAdapter;

import java.util.ArrayList;
import java.util.List;


/**
 * s
 * 功能介绍引导界面
 */
public class GuideActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {
    private ViewPager guide_viewpager;
    private List<ImageView> list_imageview;
    private GuidePageAdapter guide_Page_Adapter;
    public static int curr = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
        guide_viewpager = (ViewPager) findViewById(R.id.guide_viewpager);
        initView();
    }

    private void initView() {
        list_imageview = new ArrayList<ImageView>();
        ImageView image_View = null;
        for (int i = 1; i <= 3; i++) {
            int mipmap = getResources().getIdentifier("bg_guide_0" + i, "mipmap", getPackageName());
            image_View = new ImageView(GuideActivity.this);
            image_View.setImageResource(mipmap);
            list_imageview.add(image_View);
        }
        guide_Page_Adapter = new GuidePageAdapter(list_imageview);
        guide_viewpager.setAdapter(guide_Page_Adapter);
        guide_viewpager.setOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (2 == position) {
            guide_viewpager.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    finish();
                    Intent intent = new Intent(GuideActivity.this, MainActivity.class);
                    startActivity(intent);
                    return true;
                }
            });
//            guide_viewpager.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    finish();
//                    Intent intent = new Intent(GuideActivity.this, MainActivity.class);
//                    startActivity(intent);
//                }
//            });

        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}

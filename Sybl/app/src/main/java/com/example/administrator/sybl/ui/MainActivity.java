package com.example.administrator.sybl.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.RadioGroup;

import com.example.administrator.sybl.R;
import com.example.administrator.sybl.fragment.Activity_fragment;
import com.example.administrator.sybl.fragment.Gift_fragment;
import com.example.administrator.sybl.fragment.Money_fragment;
import com.example.administrator.sybl.fragment.Mygift_fragment;
import com.example.administrator.sybl.utils.FragmentTabUtils;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends FragmentActivity implements FragmentTabUtils.OnRgsExtraCheckedChangedListener {
    private RadioGroup main_radio;
    private List<Fragment> list;
    private Gift_fragment gift_fragment;
    private Activity_fragment activity_fragment;
    private Money_fragment money_fragment;
    private Mygift_fragment mygift_fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        main_radio = (RadioGroup) findViewById(R.id.main_radioGroup);
        list = new ArrayList<>();
        gift_fragment = new Gift_fragment();
        activity_fragment = new Activity_fragment();
        money_fragment = new Money_fragment();
        mygift_fragment = new Mygift_fragment();
        list.add(gift_fragment);
        list.add(activity_fragment);
        list.add(money_fragment);
        list.add(mygift_fragment);
        initView();
    }

    private void initView() {
        FragmentTabUtils tabUtils = new FragmentTabUtils(getSupportFragmentManager(), list, R.id.main_fragment, main_radio, MainActivity.this);
        tabUtils.setOnRgsExtraCheckedChangedListener(MainActivity.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void OnRgsExtraCheckedChanged(RadioGroup radioGroup, int checkedId, int index) {

    }
}

package com.example.administrator.sybl.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.example.administrator.sybl.R;
import com.example.administrator.sybl.adapter.Gift_adapter;
import com.example.administrator.sybl.bean.Gift;
import com.example.administrator.sybl.ui.MainActivity;
import com.example.administrator.sybl.ui.XQActivity;
import com.example.administrator.sybl.utils.HttpUtils;
import com.example.administrator.sybl.utils.UrlConstants;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 15-6-15.
 */
public class Gift_item1_fragment extends Fragment implements AdapterView.OnItemClickListener, PullToRefreshBase.OnRefreshListener2<ListView> {
    private Map<String, String> map;
    private int page = 1;
    private int gifttype = 1;
    private List<Gift> list = new ArrayList<>();
    private PullToRefreshListView ptrListView;
    private Gift_adapter gift_adapter;
    private View view;
    private ListView listView;
    private List<Gift> gifts;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_fulllistview, null);
        // View inflate = inflater.inflate(R.layout.activity_gift_item1, null);
        map = new HashMap<>();
        map.put("uid", "");
        map.put("token", "78fc45f6f%2C7%2C9");
        map.put("platform", "2");
        map.put("gifttype", "1");
        map.put("page", "" + page);
        ptrListView = (PullToRefreshListView) view.findViewById(R.id.fragment_tuan_ptrListview);
        ptrListView.setMode(PullToRefreshBase.Mode.BOTH);
        ptrListView.setOnRefreshListener(this);
        ptrListView.setOnItemClickListener(this);
        listView = ptrListView.getRefreshableView();
        //  listView.addHeaderView(headVRelativeLayoutiew);
        gift_adapter = new Gift_adapter(list, getActivity());
        ptrListView.setAdapter(gift_adapter);
        ptrListView.setRefreshing();
        return view;
    }

    private void loadData() {
        new MyTask().execute(UrlConstants.URL_GIFT);
    }


    @Override
    public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
        page = 1;
        loadData();
    }

    @Override
    public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
        page++;
        loadData();

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Gift gift = list.get((int) l);
        String id = gift.getId();
        Intent intent = new Intent(getActivity(), XQActivity.class);
        intent.putExtra("id", id);
    //    Toast.makeText(getActivity(), "wuwuwu" + intent.getStringExtra("id"), Toast.LENGTH_LONG).show();
        startActivity(intent);
    }

    public class MyTask extends AsyncTask<String, Void, List<Gift>> {

        @Override
        protected List<Gift> doInBackground(String... params) {
            try {
                String post = HttpUtils.post(params[0], map);
                JSONObject jsonObject = new JSONObject(post);
                JSONArray info = jsonObject.optJSONArray("info");
                Log.i("info", "======3=====" + info.length());
                List<Gift> gf = new ArrayList<>();
                for (int i = 0; i < info.length(); i++) {
                    Gift gift = new Gift();
                    JSONObject jsonObject1 = info.optJSONObject(i);
                    gift.setId(jsonObject1.optString("id"));
                    gift.setName(jsonObject1.optString("name"));
                    gift.setContent(jsonObject1.optString("content"));
                    gift.setIcon(jsonObject1.optString("icon"));
                    gift.setRemain(jsonObject1.optString("remain"));
                    gf.add(gift);

                }
                return gf;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Gift> strings) {

            super.onPostExecute(strings);
            if (page == 1 && !list.isEmpty()) {
                list.clear();
            }
            Log.i("info", "======2222=========" + strings);
            list.addAll(strings);
            // Log.i("info", "===============" + list);
            gift_adapter.notifyDataSetChanged();
            ptrListView.onRefreshComplete();
        }
    }

}
package com.example.administrator.sybl.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.example.administrator.sybl.R;
import com.example.administrator.sybl.utils.AppUtils;


/**
 * 欢迎界面
 */
public class SplashActivity extends AppCompatActivity {
//    private static Handler handler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            switch (msg.what) {
//                case 1:
//                    Intent intent = new Intent()
//                    break;
//
//            }
//        }
//    };


    /**
     *
     */
    private ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initView();
    }

    private void initView() {
        img = (ImageView) this.findViewById(R.id.splash_img);
        AnimationSet set = new AnimationSet(true);
        TranslateAnimation alphaAnimation = new TranslateAnimation(50f, 50f, 0f, 100.0f);
        alphaAnimation.setDuration(500);
        set.addAnimation(alphaAnimation);
        img.startAnimation(set);
        TranslateAnimation alphaAnimation2 = new TranslateAnimation(50f, 50f, 20f, 100.0f);
        alphaAnimation2.setDuration(500);
        set.addAnimation(alphaAnimation2);
        img.startAnimation(set);
        TranslateAnimation alphaAnimation3 = new TranslateAnimation(50f, 50f, 40f, 100.0f);
        alphaAnimation3.setDuration(500);
        set.addAnimation(alphaAnimation3);


        set.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //动画结束后执行

                startIntent();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        img.startAnimation(set);
    }

    private void startIntent() {
        Intent intent;
        if (AppUtils.isFirst(this)) {
            intent = new Intent(this, GuideActivity.class);
        } else {
            intent = new Intent(this, MainActivity.class);
        }
        startActivity(intent);
        this.finish();
    }

}

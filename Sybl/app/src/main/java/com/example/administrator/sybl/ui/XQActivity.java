package com.example.administrator.sybl.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.administrator.sybl.MySync;
import com.example.administrator.sybl.R;
import com.example.administrator.sybl.adapter.XQAdapter;
import com.example.administrator.sybl.bean.XQ_gift;
import com.example.administrator.sybl.utils.HttpUtils;
import com.example.administrator.sybl.utils.UrlConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 15-6-18.
 */
public class XQActivity extends Activity {
    private Map<String, String> map;
    private XQAdapter xqAdapter;
    private ListView webView;
    private List<XQ_gift> list;
    private ImageView image;
    private MySync ms;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.xq_web);
        Intent intent = getIntent();
        webView = (ListView) findViewById(R.id.web);
        image = (ImageView) findViewById(R.id.qf_image);
        String id = intent.getStringExtra("id");
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getApplication(), MainActivity.class);
                startActivity(intent1);
            }
        });
        map = new HashMap<>();
        map.put("uid", "");
        map.put("token", "a46f90a021aec5a011dbec7488a11a25");
        map.put("compare", "2e8bffa93bab9f446de91762cde60c82");
        map.put("id", id);
        new MySync(map, new MySync.VHolder() {
            @Override
            public Void send(List<XQ_gift> xq_gifts) {
                xqAdapter = new XQAdapter(XQActivity.this, xq_gifts);
                webView.setAdapter(xqAdapter);
                return null;
            }
        }).execute(UrlConstants.URL_XQ);
        Log.i("朱文杰", "==========朱文杰============" + UrlConstants.URL_XQ);
        Toast.makeText(getApplicationContext(), "..." + id, Toast.LENGTH_LONG).show();
    }


}

package com.example.administrator.sybl.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 15-6-16.
 */
public class Gift {

    /**
     * content : 铜币*2000，女儿红*2，侠客精魄礼盒*1，水判官精魄*1
     * id : 3542
     * icon : http://i3.72g.com/upload/201506/201506041344181170.jpg
     * gamename : 不良人
     * remain : 198
     * gifttype : 1
     * name : 《不良人》72G普通礼包
     * consume : 0.0
     */
    private String id;
    private String name;
    private String icon;
    private String remain;
    private String content;

    @Override
    public String toString() {
        return "Gift{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", icon='" + icon + '\'' +
                ", remain='" + remain + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getRemain() {
        return remain;
    }

    public void setRemain(String remain) {
        this.remain = remain;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

package com.example.administrator.sybl.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.administrator.sybl.R;
import com.example.administrator.sybl.adapter.Activity_adapter;
import com.example.administrator.sybl.adapter.Money_adapter;
import com.example.administrator.sybl.bean.Gift;
import com.example.administrator.sybl.bean.Item_activity;
import com.example.administrator.sybl.bean.Money;
import com.example.administrator.sybl.ui.XQActivity;
import com.example.administrator.sybl.ui.YX_activity;
import com.example.administrator.sybl.utils.HttpUtils;
import com.example.administrator.sybl.utils.UrlConstants;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 15-6-15.
 */
public class Money_fragment extends Fragment implements AdapterView.OnItemClickListener, PullToRefreshBase.OnRefreshListener2<ListView> {
    private Map<String, String> map;
    private int page = 1;
    private int gifttype = 1;
    private List<Money> list = new ArrayList<>();
    private PullToRefreshListView ptrListView;
    private Money_adapter money_adapter;
    private View view;
    private ListView listView;
    private List<Money> gifts;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.money_fulllistview, null);
        // View inflate = inflater.inflate(R.layout.activity_gift_item1, null);
        map = new HashMap<>();
        map.put("platform", "2");
        map.put("page", "" + page);

        ptrListView = (PullToRefreshListView) view.findViewById(R.id.fragment_tuan_ptrListview);
        ptrListView.setMode(PullToRefreshBase.Mode.BOTH);
        ptrListView.setOnRefreshListener(this);
        ptrListView.setOnItemClickListener(this);
        /**
         * 获取系统自带ListView
         */
        listView = ptrListView.getRefreshableView();
        //  listView.addHeaderView(headVRelativeLayoutiew);

        money_adapter = new Money_adapter(getActivity(), list);
        ptrListView.setAdapter(money_adapter);
        //设置自动刷新 需要注释掉 pulltorefreshListView 源码71-75行代码
        ptrListView.setRefreshing();
        return view;
    }

    private void loadData() {
        new MyTask().execute(UrlConstants.URL_MONEY);
    }


    @Override
    public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
        page = 1;
        loadData();
    }

    @Override
    public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
        page++;
        loadData();

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Money gift = list.get((int) l);
        Log.i("info", "========朱文杰=========" + gift);
        String id = gift.getId();
        Intent intent = new Intent(getActivity(), YX_activity.class);
        intent.putExtra("id", id);
        Toast.makeText(getActivity(), "wuwuwu" + intent.getStringExtra("id"), Toast.LENGTH_LONG).show();
        startActivity(intent);
    }


    public class MyTask extends AsyncTask<String, Void, List<Money>> {

        @Override
        protected List<Money> doInBackground(String... params) {
            try {
                String post = HttpUtils.post(params[0], map);

                JSONObject jsonObject = new JSONObject(post);
                JSONArray info = jsonObject.optJSONArray("info");

                List<Money> gf = new ArrayList<>();
                for (int i = 0; i < info.length(); i++) {
                    Money gift = new Money();
                    JSONObject jsonObject1 = info.optJSONObject(i);
                    gift.setId(jsonObject1.optString("id"));
                    gift.setName(jsonObject1.optString("name"));
                    gift.setScore(jsonObject1.optString("score"));
                    gift.setIcon(jsonObject1.optString("icon"));
                    gift.setCount_dl(jsonObject1.optString("count_dl"));
                    gift.setSize(jsonObject1.optString("size"));
                    gf.add(gift);

                }
                return gf;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Money> strings) {

            super.onPostExecute(strings);
            if (page == 1 && !list.isEmpty()) {
                list.clear();
            }
            Log.i("info", "======2222=========" + strings);
            list.addAll(strings);
            // Log.i("info", "===============" + list);
            money_adapter.notifyDataSetChanged();
            ptrListView.onRefreshComplete();
        }
    }

}

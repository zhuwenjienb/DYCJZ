package com.example.administrator.sp;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Administrator on 15-6-19.
 */
public class Start_Activity extends AppCompatActivity {


    public static Boolean getBoolean(Context context) {
        SharedPreferences num1 = context.getSharedPreferences("num1", MODE_PRIVATE);
        int version = num1.getInt("version", 0);
        boolean NB = num1.getBoolean("boolean", true);
        if (version != getVersion(context) || NB) {
            SharedPreferences.Editor edit = num1.edit();
            SharedPreferences.Editor version1 = edit.putInt("version", getVersion(context));
            version1.commit();
            SharedPreferences.Editor aBoolean = edit.putBoolean("boolean", false);
            aBoolean.commit();
        }
        return NB;
    }

    public static int getVersion(Context context) {
        int version = 0;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = packageInfo.versionCode;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

}

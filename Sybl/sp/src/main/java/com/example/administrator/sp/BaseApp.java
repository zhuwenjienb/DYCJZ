package com.example.administrator.sp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by Administrator on 15-6-19.
 */
public class BaseApp extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base);
        Toast.makeText(getApplication(), "1234", Toast.LENGTH_LONG).show();
        inintView();
    }

    private void inintView() {
        ImageView IV = (ImageView) findViewById(R.id.sp_image);
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation TA = new TranslateAnimation(50f, 100f, 50f, 100f);
        TA.setDuration(1 * 1000);
        animationSet.addAnimation(TA);
        IV.startAnimation(animationSet);
        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                endView();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    private void endView() {
        if (Start_Activity.getBoolean(this)) {
            Intent intent = new Intent(this, One_activity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }
}

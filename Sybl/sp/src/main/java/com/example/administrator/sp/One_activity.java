package com.example.administrator.sp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 15-6-19.
 */
public class One_activity extends Activity {
    private List<ImageView> list;
    private Button btn;
    private ImageView image;
    private Adapter MA;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.oneiteme_viewpager);
        ViewPager VP = (ViewPager) findViewById(R.id.sp_ViewPager);
        list = new ArrayList<>();
        btn = (Button) findViewById(R.id.btn1);
        initView();
        MA = new Adapter(list);
        VP.setAdapter(MA);
        VP.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                btn.setVisibility(View.GONE);
                if (position == 2) {
                    btn.setVisibility(View.VISIBLE);
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent intent = new Intent(One_activity.this, MainActivity.class);
                            startActivity(intent);
                        }
                    });
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initView() {
        for (int i = 1; i <= 3; i++) {
            image = new ImageView(One_activity.this);
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            image.setLayoutParams(layoutParams);
            if (i == 1) {
                image.setImageResource(getResources().getIdentifier("bg_guide_0" + i, "mipmap", getPackageName()));
            } else if (i == 2) {
                image.setImageResource(getResources().getIdentifier("bg_guide_0" + i, "mipmap", getPackageName()));
            } else if (i == 3) {
                image.setImageResource(getResources().getIdentifier("bg_guide_0" + i, "mipmap", getPackageName()));

            }
            list.add(image);
        }
    }
}

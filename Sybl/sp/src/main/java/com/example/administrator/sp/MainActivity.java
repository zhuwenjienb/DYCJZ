package com.example.administrator.sp;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RadioGroup;

import com.example.administrator.sp.Fragment.Activity_Fragment;
import com.example.administrator.sp.Fragment.Gift_Fragment;
import com.example.administrator.sp.Fragment.Money_Fragment;
import com.example.administrator.sp.Fragment.My_Fragment;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {
    private FrameLayout FL;
    private RadioGroup RB;
    private List<Fragment> list_fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar supportActionBar = getSupportActionBar();
        FL = (FrameLayout) findViewById(R.id.main_fragment);
        RB = (RadioGroup) findViewById(R.id.main_radio);
        list_fragment = new ArrayList<>();
        list_fragment.add(new Activity_Fragment());
        list_fragment.add(new Gift_Fragment());
        list_fragment.add(new Money_Fragment());
        list_fragment.add(new My_Fragment());


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
